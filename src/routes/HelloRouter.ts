import express, { Request, Response } from "express";
import { HelloController } from "@/controller/HelloController";
import { LogInfo } from "@/utils/logger";

// Router from express 
let helloRouter = express.Router();

// http://localhost:8000/api/hello?name=samtiago/  nos dice q va a recibir un parametro name, en caso de que no venga, devolvemos hello world
helloRouter.route('/')
    // GET: 
    .get( async (req: Request, res: Response) =>{
        // obtenemos query param
        let name: any = req?.query?.name;
        LogInfo(`Query Param: ${name}`);
        // Controller Instance to execute method
        const controller: HelloController = new HelloController();
        // obtener la respuesta 
        const response = await controller.getMessage(name);
        // ahora enviamos al cliente la respuesta 
        return res.send(response);
    })

// Export HelloRouter
export default helloRouter