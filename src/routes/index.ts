/**
 * Rout Router
 * Redirections to Routers
 */

import express, { Request, Response } from "express";
import helloRouter from "./HelloRouter";
import { LogSucces, LogInfo } from "@/utils/logger";


// server instance
let server = express();

// Router Instance -- se encargara de enrutar todas las peticions que le lleguen a la app
let rootRouter = express.Router();

// Activate for requests to http://localhost:8000/api;

rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api/')
    // Send Hello World 
    res.send("Welcome to my API Restful: Express + TS + Nodemons + Jest + Swagger + Mongoose")
});

server.use("/", rootRouter); // http://localhost:8000/api/
server.use('/hello', helloRouter); // http://localhost:8000/api/hello --> HelloRouter
// Add More routes to the app

export default server;

