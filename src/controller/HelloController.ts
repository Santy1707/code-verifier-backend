import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { LogSucces } from "@/utils/logger";

/**
 * La estructura que tiene que seguir la clase HelloController esta en la interfaz IHelloController
 */
export class HelloController implements IHelloController {
    
    public async getMessage(name?: string): Promise<BasicResponse> {
        LogSucces("[/api/hello] GetRequest");
        
        return{
            message: `Hello ${name || "World!"}`
        }

    }

}

